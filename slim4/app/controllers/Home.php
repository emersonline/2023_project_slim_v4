<?php

namespace app\controllers;

use app\database\models\User;
use app\classes\Validate;

class Home extends Base
{
    private $user;
    private $validate;

    public function __construct()
    {
        $this->user = new User;
        $this->validate = new Validate;
    }

    public function index($request, $response)
    {
        $users = $this->user->find();
        
        $this->validate->required(['firstName', 'email', 'password'])->exist($this->user, 'email','emersonline2007@gmail.com');

        var_dump($this->validate->getErrors());

        return $this->getTwig()->render($response, $this->setView('site/home'), [
            'title' => 'Home',
            'users' => $users,
        ]);
    }
}