<?php

namespace app\traits;

use PDOException;

trait Create{
    public function create(array $createFieldsAndValues){
        try{
            // var_dump($createFieldsAndValues);
            $sql = sprintf("insert into %s (%s) values(%s)", $this->table, implode(',', array_keys($createFieldsAndValues)), ':'.implode(',:', array_keys($createFieldsAndValues)));
            $prepared = $this->connection->prepare($sql);
            return $prepared->execute($createFieldsAndValues);
            // var_dump($sql);
        }catch(PDOException $e){
            var_dump($e->getMessage());
        }
    }
}