<?php 

namespace app\traits;

use PDOException;

trait Read
{
    public function find($fetchAll = true)
    {
        try{
            $query = $this->connection->query("select * from {$this->table}");
            return $fetchAll ? $query->fetchAll() : $query->fetch();
        }catch(PDOException $e){
            var_dump($e->getMessage());
        }
    }

    public function findBy($field, $value, $fetchAll = false){
        try{
            $prepare = $this->connection->prepare("select * from {$this->table} where {$field} = :{$field}");
            $prepare->bindValue(":{$field}", $value);
            $prepare->execute();
            return $fetchAll ? $prepare->fetchAll() : $prepare->fetch();
        }catch(PDOException $e){
            var_dump($e->getMessage());
        }
    }
}
