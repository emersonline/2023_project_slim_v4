<?php
require __DIR__ . '/../vendor/autoload.php';

use Slim\Factory\AppFactory;
use Slim\Middleware\MethodOverrideMiddleware;

$app = AppFactory::create();

require '../app/routes/site.php';
require '../app/routes/user.php';

$methodOverrideMiddleware =  new MethodOverrideMiddleware();
$app->add($methodOverrideMiddleware);

/* 
rota não encontrada cai aqui! 
var_dump("teste"); 
*/
// TRATAMENTO PARA ROTAS NÃO ENCONTRADAS
$app->map(['GET', 'POST', 'PUT', 'DELETE', 'PATCH'], '/{routes:.+}', function($request, $response){
    $response->getBody()->write('Endereço não existe!');
    return $response;
});

$app->run();